const webpack = require('webpack');
const path = require('path');
module.exports = {
    entry: [
        'react-hot-loader/patch',
        './src/index.js',
        'babel-polyfill'
    ],
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        }
      ]
    },
    resolve: {
      alias: {
        Containers: path.resolve(__dirname, 'src/containers/'),
      },
        extensions: ['*', '.js', '.jsx','.css']
    },
    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: 'bundle.js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        contentBase: './dist',
        hot: true
    }

  };